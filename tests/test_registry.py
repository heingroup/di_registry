import json
import pathlib
import os
import responses
from typing import Dict
from unittest import TestCase
from di_registry import registry
from di_registry.registry import RegistryError

conditional_path = pathlib.Path(os.path.dirname(os.path.abspath(__file__)))


class TestClass:
    foo = 'bar'

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class TestClass2:
    foo = 'baz'

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class TestRegistry(TestCase):
    def setUp(self) -> None:
        registry.Registry.reset()

    def test_get_default_registry(self):
        self.assertIsNone(registry.Registry._default_registry)
        reg = registry.Registry.default_registry()
        reg2 = registry.default_registry()
        self.assertIs(reg, reg2, registry.Registry._default_registry)

    def test_register(self):
        reg = registry.Registry()
        reg.register(TestClass, 'test')
        self.assertIn('test', reg.factories)
        self.assertIn(TestClass, reg.factory_types)
        registry.register(TestClass, 'test2')
        self.assertIn('test2', registry.default_registry().factories)
        self.assertIn(TestClass, registry.default_registry().factory_types)

    def test_get_by_name(self):
        registry.register(TestClass, 'test')
        test = registry.default_registry().get_by_name('test')
        self.assertIsInstance(test, TestClass)

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.default_registry().get_by_name('foobar')

    def test_get_by_type(self):
        registry.register(TestClass)
        test = registry.default_registry().get_by_type(TestClass)
        self.assertIsInstance(test, TestClass)

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.default_registry().get_by_type(TestClass2)

    def test_get(self):
        registry.register(TestClass)
        registry.register(TestClass2, 'test')

        test = registry.get(TestClass)
        test2 = registry.get('test')

        self.assertIsInstance(test, TestClass)
        self.assertIsInstance(test2, TestClass2)

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.get('foobar')

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.get(TestRegistry)

    def test_configure_dict(self):
        registry.configure({
            'test': ['arg1', 'arg2'],
            'TestClass2': {
                '$args': ['arg1'],
                'foo': 'bar'
            }
        })

        registry.register(TestClass, 'test')
        registry.register(TestClass2)

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertListEqual(list(test.args), ['arg1', 'arg2'])

        test2: TestClass2 = registry.get(TestClass2)
        self.assertIsInstance(test2, TestClass2)
        self.assertListEqual(list(test2.args), ['arg1'])
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    def test_configure_merging(self):
        # first config
        registry.configure({
            'shallow': {
                'foo': ['first']
            },
            'deep': {
                'foo': { 'bar': ['first'] }
            }
        })
        # second config will be merged
        registry.configure({
            'shallow': {
                'foo': ['second']
            },
            'deep': {
                'foo': {'bar': ['second']}
            }
        })

        shallow = registry.get_configuration('shallow')
        deep = registry.get_configuration('deep')

        # lists in second config should overwrite, not append first config
        self.assertEqual(['second'], shallow['foo'])
        self.assertEqual(['second'], deep['foo']['bar'])

    def test_configure_json(self):
        registry.configure(conditional_path / 'files/config.json')

        registry.register(TestClass, 'test')
        registry.register(TestClass2)

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertListEqual(list(test.args), ['arg1', 'arg2'])

        registry.default_registry().reset_object('test')
        test: TestClass = registry.get('test', 'arg3', foo='bar')
        self.assertListEqual(list(test.args), ['arg1', 'arg2', 'arg3'])

        test2: TestClass2 = registry.get(TestClass2)
        self.assertIsInstance(test2, TestClass2)
        self.assertListEqual(list(test2.args), ['arg1'])
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    def test_configure_yaml(self):
        registry.configure(conditional_path / 'files/config.yaml')

        registry.register(TestClass, 'test')
        registry.register(TestClass2)

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertListEqual(list(test.args), ['arg1', 'arg2'])

        registry.default_registry().reset_object('test')
        test: TestClass = registry.get('test', 'arg3', foo='bar')
        self.assertListEqual(list(test.args), ['arg1', 'arg2', 'arg3'])

        test2: TestClass2 = registry.get(TestClass2)
        self.assertIsInstance(test2, TestClass2)
        self.assertListEqual(list(test2.args), ['arg1'])
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    @responses.activate
    def test_configure_http(self):
        responses.add(responses.GET, 'http://foobar/myservice', json={'test': {'foo': 'bar'}})
        registry.register(TestClass, 'test')
        registry.register(TestClass, 'test2')
        registry.configure('http://foobar/myservice')

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertDictEqual(test.kwargs, {'foo': 'bar'})

        # ensure connection errors are handled properly
        with self.assertRaises(RegistryError):
            registry.configure('http://badhost')

        # ensure 404s are handled properly
        responses.add(responses.GET, 'http://foobar/otherservice', status=404)
        with self.assertRaises(RegistryError):
            registry.configure('http://foobar/otherservice')

        # 404s can be ignored
        registry.configure('http://foobar/otherservice', ignore_missing=True)

        # ensure bad responses handled
        responses.add(responses.GET, 'http://foobar/anotherservice', status=500)
        with self.assertRaises(RegistryError):
            registry.configure('http://foobar/anotherservice', ignore_missing=True)

        # ensure bad JSON responses handled
        responses.add(responses.GET, 'http://foobar/lastservice', body='foobar')
        with self.assertRaises(RegistryError):
            registry.configure('http://foobar/lastservice', ignore_missing=True)

        # ensure `http_configure` works
        with self.assertRaises(RegistryError):
            registry.http_configure('httpservice')

        registry.configure(conditional_path / 'files/config_http.yaml')
        responses.add(responses.GET, 'http://foobar/httpservice', json={'test2': {'foo': 'bar'}})
        registry.http_configure('httpservice')
        test2: TestClass = registry.get('test2')
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    @responses.activate
    def test_http_saving(self):
        """tests saving parameters to an HTTP endpoint"""
        def save_callback(request):
            self.assertDictEqual(json.loads(request.body), {'foo': 'bar'})
            return 200, {}, b''

        responses.add_callback(responses.PUT,
                               'http://foobar/httpservice',
                               callback=save_callback,
                               content_type='application/json')

        responses.add(responses.PUT, 'http://foobar/failed', status=500)

        registry.configure(conditional_path / 'files/config_http.yaml')
        registry.save_http_configuration('httpservice', {'foo': 'bar'})

        with self.assertRaises(RegistryError):
            registry.save_http_configuration('failed', {'foo': 'bar'})

    def test_type_key(self):
        registry.configure(conditional_path / 'files/config.yaml')

        registry.register(TestClass2)

        test = registry.get(TestClass2)
        foo = registry.get('foo')

        self.assertIsInstance(test, TestClass2)
        self.assertIsInstance(foo, TestClass2)
        self.assertIsNot(test, foo)
        self.assertDictEqual(test.kwargs, {'foo': 'bar'})
        self.assertDictEqual(foo.kwargs, {'foo': 'baz'})

    def test_multiple_config(self):
        registry.configure(conditional_path / 'files/parent_config.yaml')
        registry.configure(conditional_path / 'files/child_config.yaml')

        registry.register(TestClass)

        parent = registry.get(TestClass)
        child = registry.get('test')

        self.assertIsInstance(parent, TestClass)
        self.assertIsInstance(child, TestClass)
        self.assertIsNot(parent, child)
        self.assertDictEqual(parent.kwargs, {'root': 'TestClass', 'parent': ''})
        self.assertDictEqual(child.kwargs, {'root': 'TestClass', 'parent': 'TestClass', 'foo': 'baz'})

    def test_config_object_injection(self):
        registry.configure(conditional_path / 'files/injection.yaml')

        registry.register(TestClass)
        registry.register(TestClass2)

        main_instance: TestClass2 = registry.get('main_instance')

        self.assertIsInstance(main_instance, TestClass2)
        self.assertIsInstance(main_instance.kwargs['injected'], TestClass)
        self.assertDictEqual(main_instance.kwargs['injected'].kwargs, {'info': 'injected'})

    def test_config_value_injection(self):
        registry.configure(conditional_path / 'files/config_value.yaml')
        registry.register(TestClass)

        test: TestClass = registry.get(TestClass)
        self.assertIsInstance(test, TestClass)
        self.assertDictEqual(test.kwargs, {'foo': 'bar', 'baz': 'foobar'})

    def test_environment_variable_injection(self):
        registry.configure(conditional_path / 'files/config_env.yaml')

        os.environ['foo'] = 'foobar'

        args, kwargs = registry.default_registry().get_object_configuration('foo')
        self.assertEqual(kwargs['bar'], 'foobar')

        args, kwargs = registry.default_registry().get_object_configuration('bar')
        self.assertEqual(kwargs['foo'], '')
        self.assertEqual(kwargs['url_default'], 'https://mqtt.eclipseprojects.io')
        self.assertEqual(kwargs['uuid_default'], '4f864017-b8ea-4b7d-8657-6280e91d6701')
        self.assertEqual(kwargs['number_default'], 1234)
        self.assertEqual(kwargs['bool_default'], False)