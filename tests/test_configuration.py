"""tests configuration and loading via YAML"""
import copy
import os
import pathlib
import unittest

import yaml
import random

from di_registry.abc import YAMLParam, YAMLConfig
from di_registry import registry

conditional_path = pathlib.Path(os.path.dirname(os.path.abspath(__file__)))


def retrieve_contents(fn) -> dict:
    """Retrieves the contents of the yaml"""
    with open(fn) as f:
        out = yaml.safe_load(f)
    return out


class SupportsNull(YAMLConfig):
    YAML_TREE_NAME = 'behaviour'

    yaml_parameters = {
        'no_null_support': YAMLParam(1, 'this parameter does not support null overrides', int, allow_null_values=False),
        'null_support': YAMLParam(1, 'this parameter supports null overrides', int, allow_null_values=True),
        'default': YAMLParam('asdf', 'this parameter used to test default', str),
        'user_modified': YAMLParam(123., 'this parameter used to test user override', float),
        'user_modified_str': YAMLParam('asdf', 'this parameter used to test user override for strings', str),
        'aliased': YAMLParam('asdf', 'this parameter has an alias', kwarg_alias='alias_name'),
        'not_included': YAMLParam('asdf', 'this parameter should not be included', include=False),
        'typecasting': YAMLParam('1234', 'this parameter should be type cast', int),
        'boolean': YAMLParam(True, 'this parameter should be a boolean', bool),
    }

    def __init__(self):
        self.no_null_support = 1
        self.null_support = 2
        self.default = None
        self.user_modified = random.random()
        self.user_modified_str = ''
        self.boolean = True


class TestYAMLBehaviour(unittest.TestCase):
    fn = 'null_support_test.yaml'

    @classmethod
    def setUpClass(cls) -> None:
        cls.inst = SupportsNull()
        # adjust values
        cls.inst.no_null_support = None
        cls.inst.null_support = None
        cls.inst.boolean = False
        cls.rnd_user_val = random.random()
        cls.rnd_user_str = random.choice(['cat', 'dog', 'rabbit'])
        cls.inst.user_modified = cls.rnd_user_val
        cls.inst.user_modified_str = cls.rnd_user_str
        cls.inst.save_parameters_to_yaml(cls.fn, mode='w')
        cls.loaded = retrieve_contents(cls.fn)
        registry.configure(conditional_path / 'files/config_http.yaml')

    def get_value(self, param):
        """retrieves a parameter from the loaded dictionary"""
        # direct accession used to catch undefined events
        return self.loaded[SupportsNull.YAML_TREE_NAME][param]

    def test_tree_name(self):
        """tests that the tree name is saved properly"""
        self.assertIn(
            SupportsNull.YAML_TREE_NAME,
            self.loaded
        )

    def test_null_allowed(self):
        """tests that the null-allowed value was saved correctly"""
        self.assertIsNone(
            self.get_value('null_support'),
            'check that the null-supporting output was saved'
        )

    def test_null_disallowed(self):
        """tests null disallowed saving"""
        val = self.get_value('no_null_support')
        self.assertIsNotNone(val)
        self.assertEqual(val, 1)

    def test_default(self):
        """tests usage of the default value"""
        val = self.get_value('default')
        self.assertIsNotNone(val)
        self.assertEqual(val, SupportsNull.yaml_parameters['default'].default_value)

    def test_user_modified(self):
        """tests the user modified value"""
        val = self.get_value('user_modified')
        self.assertEqual(val, self.rnd_user_val)

    def test_aliasing(self):
        """tests aliasing"""
        self.assertNotIn('aliased', self.loaded, 'ensures that alias is not included')
        val = self.get_value('alias_name')
        self.assertEqual(val, 'asdf')

    def test_exclusion(self):
        """tests exclusion of a parameter from the yaml"""
        self.assertNotIn('not_included', self.loaded)

    def test_typecasting(self):
        """tests the automatic type-casting"""
        self.assertTrue(type(self.get_value('typecasting')) is int)

    def test_apply_parameters(self):
        """test applying parameters to an instance"""
        # test applying a value to a parameter
        self.inst.apply_parameters(**{'user_modified': 3})
        self.assertEqual(self.inst.user_modified, 3)

        # test applying None a parameter
        self.inst.apply_parameters(**{'user_modified': None})
        self.assertEqual(self.inst.user_modified, None)

    def test_collision_avoidance(self):
        """tests that changing one class instance does not affect the others"""
        one = SupportsNull()
        two = SupportsNull()
        one.yaml_parameters['default'].default_value = 'qwer'
        self.assertNotEqual(
            one.yaml_parameters['default'].default_value,
            two.yaml_parameters['default'].default_value,
            'ensures that modifying the default value of one class did not affect the parameters of the other'
        )

    def test_string_literals(self):
        """test saving a string parameters with characters that 'shouldnt' be allowed, like colons"""
        self.inst.apply_parameters(**{'user_modified_str': 'asdf : asdf'})
        self.assertEqual(self.inst.user_modified_str, 'asdf : asdf',
                         'ensure instance string attribute could be set with a : in it')

        self.inst.save_parameters_to_yaml(self.fn, mode='w')
        loaded = retrieve_contents(self.fn)
        contents = loaded[SupportsNull.YAML_TREE_NAME]
        self.assertEqual(contents['user_modified_str'], 'asdf : asdf',
                         'ensure retrieving a string value with a colon from the yaml file works')

    def test_default_overrides(self):
        """tests saving to file providing overrides"""
        overrides = {
            'no_null_support': 2,
            'null_support': 3,
            'default': 'asdfqwer',
            'user_modified': 123.0,
            'user_modified_str': 'asaasdfdf',
            'aliased': 'asqwerdf',
            'not_included': 'asdfhhgf',
            'typecasting': '12344321',
            'boolean': False,
        }
        expected = copy.copy(overrides)
        expected['typecasting'] = int(overrides['typecasting'])
        expected['alias_name'] = overrides['aliased']
        del expected['aliased']
        SupportsNull.save_default_parameters_to_yaml('overridden.yaml', 'w', **overrides)
        vals = retrieve_contents('overridden.yaml')
        self.assertEqual(
            expected,
            vals[SupportsNull.YAML_TREE_NAME]
        )

    def test_get_kwargs(self):
        """tests retrieval of keyword arguments from the instance"""
        kwargs = self.inst.get_current_kwargs()
        expected = {'no_null_support': 1, 'null_support': None, 'default': 'asdf', 'user_modified': 0.9064843122861742,
             'user_modified_str': 'cat', 'alias_name': 'asdf', 'typecasting': 1234, 'boolean': False}
        for key, val in expected.items():
            self.assertIn(key, kwargs)
            if key in ['user_modified_str', 'user_modified']:
                continue
            self.assertEqual(val, kwargs[key], f'check value for {key}')

    def test_get_default_kwargs(self):
        """tests retrieval of default kwargs from the instance"""
        default_kwargs = SupportsNull.get_default_kwargs()
        self.assertEqual(
            {'no_null_support': 1, 'null_support': 1, 'default': 'asdf', 'user_modified': 123.0,
             'user_modified_str': 'asdf', 'alias_name': 'asdf', 'not_included': 'asdf', 'typecasting': 1234,
             'boolean': True},
            default_kwargs
        )


class SubConfiguration(YAMLConfig):
    yaml_parameters = {
        'param_one': YAMLParam(123, 'first parameter', typecast=int),
        'param_two': YAMLParam(456, 'second parameter which shares the same name as the superconfiguration', typecast=int),
        'param_four': YAMLParam('regular value', 'fourth parameter that is sensitive', typecast=str),
    }

    def __init__(self,
                 param_one,
                 param_two,  # intentional collision between parameter names
                 param_four = yaml_parameters['param_four'].default_value,
                 ):
        """representative class which will be a subconfiguration of another instance"""
        self.param_one = param_one
        self.param_two = param_two
        self._p4 = None
        self.param_four = param_four

    @property
    def param_four(self) -> str:
        return self._p4

    @param_four.setter
    def param_four(self, value: str):
        if value == 'some invalid value':
            raise ValueError(f'user tried to set an invalid value for param_four')
        self._p4 = value


class HasSubConfigurations(YAMLConfig):
    sub_configurations = {
        'sub_configuration': SubConfiguration,
    }
    sub_configuration_aliases = {
        'sub_configuration': {
            'param_two': 'sub_param_two',  # maps the parameter "param_two" to the kwarg "sub_param_two"
        },
    }

    YAML_TREE_NAME = 'HasSubConfigurations'
    yaml_parameters = {
        'param_two': YAMLParam('asdf', 'second parameter', typecast=str),
    }

    def __init__(self,
                 param_two,
                 sub_param_two,  # catch for "param_two" of the sub_configuration
                 **kwargs,
                 ):
        """represents a class that has attributes which have yaml configurations"""
        # store parameter two
        self.param_two = param_two
        # instantiate subconfiguration
        self.sub_configuration = SubConfiguration(
            param_two=sub_param_two,
            **kwargs,
        )
        YAMLConfig.__init__(self)


class TestSubconfigurations(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.target_file = 'sub_configuration.yaml'
        HasSubConfigurations.save_default_parameters_to_yaml(cls.target_file, 'w')
        cls.loaded = retrieve_contents(cls.target_file)

    def test_tree_name(self):
        """tests that the tree name was set correctly"""
        self.assertIn(HasSubConfigurations.YAML_TREE_NAME, self.loaded, 'ensure tree name encompasses everything')

    def test_consolidated(self):
        """tests that the parameters were consolidated correctly into a single tree"""
        self.assertEqual(len(self.loaded), 1, 'ensure parameters were not split up')

    def test_default_values(self):
        """tests that the default values were correctly saved"""
        self.assertEqual(
            self.loaded[HasSubConfigurations.YAML_TREE_NAME],
            {'param_two': 'asdf', 'param_one': 123, 'sub_param_two': 456, 'param_four': 'regular value'},
            'ensure parameters were correctly saved'
        )

    def test_instantiation(self):
        """ensures that the class can be instantiated from the default values"""
        instance = HasSubConfigurations(**self.loaded[HasSubConfigurations.YAML_TREE_NAME])
        self.assertIsInstance(instance, HasSubConfigurations)
        self.assertIsInstance(instance.sub_configuration, SubConfiguration)

    def test_alias_setting(self):
        """tests keyword argument alias setting"""
        instance = HasSubConfigurations(**self.loaded[HasSubConfigurations.YAML_TREE_NAME])
        self.assertEqual(instance.sub_configuration.yaml_parameters['param_two'].kwarg_alias, 'sub_param_two')

    def test_live_saving(self):
        """tests saving from a live instance"""
        instance = HasSubConfigurations(**self.loaded[HasSubConfigurations.YAML_TREE_NAME])
        yaml_name = 'live_subconfig.yaml'
        instance.save_parameters_to_yaml(yaml_name, 'w')
        loaded = retrieve_contents(yaml_name)
        self.assertIn(HasSubConfigurations.YAML_TREE_NAME, loaded)
        self.assertEqual(
            loaded[HasSubConfigurations.YAML_TREE_NAME],
            {'param_four': 'regular value', 'param_one': 123, 'param_two': 'asdf', 'sub_param_two': 456},
        )

    def test_get_kwargs(self):
        """tests retrieval of kwargs"""
        instance = HasSubConfigurations(**self.loaded[HasSubConfigurations.YAML_TREE_NAME])
        kwargs = instance.get_current_kwargs()
        self.assertEqual(
            {'param_four': 'regular value', 'param_one': 123, 'param_two': 'asdf', 'sub_param_two': 456},
            kwargs
        )

    def test_get_default_kwargs(self):
        """tests retrieval of default kwargs"""
        default_kwargs = HasSubConfigurations.get_default_kwargs()
        self.assertEqual(
            {'param_four': 'regular value', 'param_one': 123, 'param_two': 'asdf', 'sub_param_two': 456},
            default_kwargs,
        )
        instance = HasSubConfigurations(**default_kwargs)

    def test_parameter_application(self):
        """tests parameter application to sub-configurations"""
        instance = HasSubConfigurations(**self.loaded[HasSubConfigurations.YAML_TREE_NAME])
        instance.apply_parameters(
            sub_param_two=789
        )
        self.assertEqual(
            instance.sub_configuration.param_two,
            789,
            'ensure that kwarg alias was used correctly to pass to sub configuration'
        )
        instance.apply_parameters(param_two='yuio')
        self.assertEqual(
            instance.param_two,
            'yuio',
            'ensure shadowed parameter name is still accepted'
        )
        self.assertNotEqual(
            instance.sub_configuration.param_two,
            'yuio',
            'ensure that sub-configuration parameter was unaffected'
        )

    def test_pydantic_export(self):
        """tests the export helper which creates a pydantic model from a YAMLConfig"""
        model = SubConfiguration.get_pydantic_model()
        default_kwargs = SubConfiguration.get_default_kwargs()
        schema = model.schema()
        self.assertEqual(
            f'{SubConfiguration.__name__}Model',
            schema['title'],
            'check schema naming'
        )
        for name, val in default_kwargs.items():
            self.assertIn(name, schema['properties'])
            schema_val = schema['properties'][name]
            self.assertEqual(
                schema_val['title'],
                name,
                'name was assigned to title'
            )
            self.assertEqual(
                schema_val['default'],
                val,
                f'check equality of {name}'
            )
        for name, param in SubConfiguration.yaml_parameters.items():
            # this will break for things that have kwargs (it's a hacky check for the docstring passthrough)
            self.assertEqual(
                schema['properties'][name]['description'],
                param.docstring,
            )

    def test_pydantic_hint_export(self):
        """tests the pydantic hint model export"""
        model = SubConfiguration.get_pydantic_hint_model()
        schema = model.schema()
        self.assertEqual(
            f'{SubConfiguration.__name__}Model',
            schema['title'],
            'check schema naming'
        )
        for param, dct in schema['properties'].items():
            self.assertEqual(
                dct['description'],
                SubConfiguration.yaml_parameters[param].docstring + f' (default: {SubConfiguration.yaml_parameters[param].default_value})',
                f'check description and default value hint for {param}'
            )


class HasYAMLAtributes(YAMLConfig):
    YAML_TREE_NAME = 'HasYAMLAtributes'
    yaml_parameters = {
        'param_two': YAMLParam('asdf', 'second parameter', typecast=str),
        'param_three': YAMLParam(1.234, 'thing that must be a float', typecast=float),
    }

    yaml_attributes = {
        'yaml_attribute': SubConfiguration,
    }

    def __init__(self,
                 param_two: str = 'asdf',
                 param_three: float = yaml_parameters['param_three'].default_value,
                 yaml_attribute=None,
                 ):
        YAMLConfig.__init__(self)
        self.param_two = param_two
        self.param_three = param_three
        self.yaml_attribute = SubConfiguration(**yaml_attribute or {})


class TestYAMLAttributes(unittest.TestCase):
    def test_yaml_attributes(self):
        kwargs = HasYAMLAtributes.get_default_kwargs()
        self.assertEqual(
            {'param_three': 1.234, 'param_two': 'asdf', 'yaml_attribute': {'param_four': 'regular value', 'param_one': 123, 'param_two': 456}},
            kwargs,
        )

    def test_default_string_list(self):
        string = HasYAMLAtributes.get_default_parameter_string_list()
        self.assertEqual(
            [
                '# second parameter',
                'param_two: "asdf"',
                '',
                '# thing that must be a float',
                'param_three: 1.234',
                '',
                'yaml_attribute: ',
                '  # first parameter',
                '  param_one: 123',
                '  ',
                '  # second parameter which shares the same name as the superconfiguration',
                '  param_two: 456',
                '  ',
                '  # fourth parameter that is sensitive',
                '  param_four: "regular value"',
                '  ',
                '  ',
                ''
            ],
            string
        )

    def test_string_list(self):
        inst = HasYAMLAtributes(
            **{
                'param_two': 'banana',
                'yaml_attribute': {
                    'param_one': 789,
                    'param_two': 275,
                }
            }
        )
        string = inst.get_parameter_string_list()
        self.assertEqual(
            [
                '# second parameter',
                'param_two: "banana"',
                '',
                '# thing that must be a float',
                'param_three: 1.234',
                '',
                'yaml_attribute: ',
                '  # first parameter',
                '  param_one: 789',
                '  ',
                '  # second parameter which shares the same name as the superconfiguration',
                '  param_two: 275',
                '  ',
                '  # fourth parameter that is sensitive',
                '  param_four: "regular value"',
                '  ',
                '  ',
                ''
            ],
            string,
        )

    def test_save_load(self):
        target_file = 'has_yaml_attributes.yaml'
        HasYAMLAtributes.save_default_parameters_to_yaml(target_file, mode='w')
        loaded_yaml = retrieve_contents(target_file)
        self.assertEqual(
            {'HasYAMLAtributes': {'param_three': 1.234, 'param_two': 'asdf', 'yaml_attribute': {'param_four': 'regular value', 'param_one': 123, 'param_two': 456}}},
            loaded_yaml
        )
        inst = HasYAMLAtributes(**loaded_yaml['HasYAMLAtributes'])

    def test_current_kwargs(self):
        kwargs = {
            'param_two': 'apple',
            'yaml_attribute': {
                'param_one': 1513,
                'param_two': 88777,
            }
        }
        inst = HasYAMLAtributes(
            **kwargs
        )
        self.assertEqual(
            {'param_two': 'apple', 'param_three': 1.234, 'yaml_attribute': {'param_one': 1513, 'param_two': 88777, 'param_four': 'regular value'}},
            inst.get_current_kwargs()
        )

    def test_apply_parameters(self):
        kwargs = {
            'param_two': 'apple',
            'yaml_attribute': {
                'param_one': 1513,
                'param_two': 88777,
            }
        }
        inst = HasYAMLAtributes(
            **kwargs
        )
        new_values = {'param_two': 'mango', 'param_three': 1.234, 'yaml_attribute': {'param_one': 4321, 'param_two': 9837, 'param_four': 'regular value'}}

        inst.apply_parameters(**new_values)
        self.assertEqual(
            new_values,
            inst.get_current_kwargs(),
        )

    def test_apply_parameters_with_error(self):
        kwargs = {
            'param_two': 'durian',
            'param_three': 'not a float',  # this should raise an error
        }
        inst = HasYAMLAtributes(**{
            'param_two': 'apple',
            'yaml_attribute': {
                'param_one': 1513,
                'param_two': 88777,
            }
        })
        with self.assertRaises(ValueError, msg='check that an error is raised if indicated'):
            inst.apply_parameters(_raise_on_error=True, **kwargs)

    def test_apply_parameters_error_no_raise(self):
        kwargs = {
            'param_two': 'durian',
            'param_three': 'not a float',  # this should raise an error
            'yaml_attribute': {'param_four': 'some invalid value'}  # this will raise an error with the sub configuration
        }
        inst = HasYAMLAtributes(**{
            'param_two': 'apple',
            'yaml_attribute': {
                'param_one': 1513,
                'param_two': 88777,
            }
        })
        errors = inst.apply_parameters_no_raise(**kwargs)
        self.assertNotIn('param_two', errors, 'check that param two applied correctly')
        self.assertIn('param_three', errors, 'check that an error was registered with param three')
        self.assertIsInstance(errors['param_three'], Exception, 'check that an exception was returned')
        self.assertEqual(
            "could not convert string to float: 'not a float'",
            str(errors['param_three']),

        )
        self.assertIn('yaml_attribute', errors)
        self.assertIsInstance(errors['yaml_attribute'], dict, 'check that dict is structured correctly')
        self.assertIn('param_four', errors['yaml_attribute'])  # todo fix type hint
        self.assertEqual(
            'user tried to set an invalid value for param_four',
            str(errors['yaml_attribute']['param_four']),
        )


def validate_super_special(value: float):
    """super special validator for a super special yaml parameter"""
    if type(value) is not float:
        value = float(value)
    if (0. < value <= 5.) is False:
        raise ValueError(f'the value {value} is not within the allowable range of 0-5')
    return value


class SpecialValidations(YAMLConfig):
    yaml_parameters = {
        'super_special': YAMLParam(1.23, typecast=validate_super_special),
        'inty': YAMLParam(1, typecast=int),
        'stringy': YAMLParam('asdf', typecast=str),
        'aliased': YAMLParam(1., typecast=float, kwarg_alias='alias_name')
    }
    # give it some sub configurations
    sub_configurations = {
        'sub_configuration': SubConfiguration,
    }
    sub_configuration_aliases = {
        'sub_configuration': {
            'param_two': 'sub_param_two',  # maps the parameter "param_two" to the kwarg "sub_param_two"
        },
    }

    def __init__(self,
                 super_special,
                 inty,
                 stringy,
                 sub_param_two,
                 **kwargs,
                 ):
        self.super_special = super_special
        self.inty = inty
        self.stringy = stringy
        self.sub_configuration = SubConfiguration(
            param_two=sub_param_two,
            **kwargs,
        )
        YAMLConfig.__init__(self)


class TestParameterValidation(unittest.TestCase):
    """tests YAMLConfig parameter validation"""
    def test_top_level(self):
        """tests top level parameter validation"""
        dct = SpecialValidations.validate_parameters(
            super_special=5.1,  # out of range
            inty='asdf',  # value error
            stringy='1234.56',  # should be fine
            alias_name='qwer',  # alias for "aliased"; value error
        )
        self.assertIn('super_special', dct)
        self.assertEqual(
            dct['super_special'],
            'the value 5.1 is not within the allowable range of 0-5',
        )
        self.assertIn('inty', dct)
        self.assertEqual(
            dct['inty'],
            'invalid literal for int() with base 10: \'asdf\''
        )
        self.assertIn('alias_name', dct)
        self.assertEqual(
            dct['alias_name'],
            'could not convert string to float: \'qwer\''
        )
        self.assertNotIn('stringy', dct)

    def test_sub_configuration_validation(self):
        """tests that sub configurations are checked against validation"""
        dct = SpecialValidations.validate_parameters(
            param_two='asdf',  # value error
        )
        self.assertIn('param_two', dct)
        self.assertEqual(
            dct['param_two'],
            'invalid literal for int() with base 10: \'asdf\''
        )
