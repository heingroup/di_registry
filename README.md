DI Registry
===========

`di-registry` is a package that provides a basic object registry that can be used for configuration or dependency injection.